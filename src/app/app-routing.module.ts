import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IdeaPageComponent} from "@components/left-container/idea-page/idea-page.component";
import {HomePageComponent} from "@components/left-container/home-page/home-page.component";
import {StructurePageComponent} from "@components/left-container/structure-page/structure-page.component";
import {
    ImplementationPageComponent
} from "@components/left-container/implementation-page/implementation-page.component";
import {
    InfrastructurePageComponent
} from "@components/left-container/infrastructure-page/infrastructure-page.component";
import {InterfacesPageComponent} from "@components/left-container/interfaces-page/interfaces-page.component";
import {FirmwarePageComponent} from "@components/left-container/firmware-page/firmware-page.component";
import {ServerPageComponent} from "@components/left-container/server-page/server-page.component";
import {ClientPageComponent} from "@components/left-container/client-page/client-page.component";
import {HardwarePageComponent} from "@components/left-container/hardware-page/hardware-page.component";
import {UpcomingPageComponent} from "@components/left-container/upcoming-page/upcoming-page.component";

const routes: Routes = [
    { path: '', component: HomePageComponent, data: { animation: 'openClosePage' } },
    { path: 'idea', component: IdeaPageComponent, data: { animation: 'openClosePage' }},
    { path: 'structure', component: StructurePageComponent, data: { animation: 'openClosePage' }},
    { path: 'implementation', component: ImplementationPageComponent, data: { animation: 'openClosePage' }},
    { path: 'infrastructure', component: InfrastructurePageComponent, data: { animation: 'openClosePage' }},
    { path: 'interfaces', component: InterfacesPageComponent, data: { animation: 'openClosePage' }},
    { path: 'upcoming', component: UpcomingPageComponent, data: { animation: 'openClosePage' }},
    { path: 'firmware', component: FirmwarePageComponent, data: { animation: 'openClosePage' }},
    { path: 'server', component: ServerPageComponent, data: { animation: 'openClosePage' }},
    { path: 'client', component: ClientPageComponent, data: { animation: 'openClosePage' }},
    { path: 'hardware', component: HardwarePageComponent, data: { animation: 'openClosePage' }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
