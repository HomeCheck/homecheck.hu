import {Component, EventEmitter, Output} from '@angular/core';
import {MainMenuItem} from "@components/main-menu/main-menu.component";
import {Router} from "@angular/router";

@Component({
    selector: 'app-mobile-main-menu',
    templateUrl: './mobile-main-menu.component.html',
    styleUrls: ['./mobile-main-menu.component.scss']
})
export class MobileMainMenuComponent {

    @Output() menuHideEvent = new EventEmitter();

    constructor(private router: Router) { }

    menuItems: MainMenuItem[] = [
        {
            title: 'Home',
            description: "",
            link: '/',
            imageUrl: 'assets/icons/user.svg',
            className: 'slide-in-from-right'
        },{
            title: 'Idea',
            description: "Why it is worth to make it and what to achieve. Also explain the benefits of the project and how it will help you to progress understanding home automation.",
            link: '/idea',
            imageUrl: 'assets/icons/idea.svg',
            className: 'slide-in-from-left menu-delay1'
        }, {
            title: 'Structure',
            description: "How it works, what it does and how. How components are connected and what they do. ",
            link: '/structure',
            imageUrl: 'assets/icons/structure.svg',
            className: 'slide-in-from-right menu-delay2'
        }, {
            title: 'Implementation',
            description:
                "Check the code, look at the details, explained. Want to extend it? No problem, just follow the instructions.",
            link: '/implementation',
            imageUrl: 'assets/icons/implementation.svg',
            className: 'slide-in-from-left menu-delay3'
        }, {
            title: 'Infrastructure',
            description: "The underlying infrastructure that makes it all work. Also the CI/CD pipeline which is also  hosted locally instead of using cloud services.",
            link: '/infrastructure',
            imageUrl: 'assets/icons/infrastructure.svg',
            className: 'slide-in-from-right menu-delay4'
        }, {
            title: 'Interfaces',
            description: " Explore the intricate connections enabling smooth communication within your smart home, including how devices interact with servers and how servers facilitate real-time communication with mobile and web clients.",
            link: '/interfaces',
            imageUrl: 'assets/icons/interfaces.svg',
            className: 'slide-in-from-left menu-delay5'
        }, {
            title: 'Upcoming',
            description: "Explore what's on the horizon for our smart home project, where we'll share our upcoming ideas, improvements, and  plans to enhance our DIY smart home experience.",
            link: '/upcoming',
            imageUrl: 'assets/icons/upcoming.svg',
            className: 'slide-in-from-right menu-delay6'
        },
    ]

    hideMobileMainMenu() {
        this.menuHideEvent.emit();
    }

    onItemClick($event: any, link: string) {
        this.router.navigate([link]);
        this.hideMobileMainMenu();
    }
}
