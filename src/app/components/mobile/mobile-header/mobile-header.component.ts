import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-mobile-header',
    templateUrl: './mobile-header.component.html',
    styleUrls: ['./mobile-header.component.scss']
})
export class MobileHeaderComponent {
    menuShown: boolean = false;

    constructor(private router: Router) {
    }

    onLogoClick() {
        this.menuShown = false;
        console.info('onLogoTap');
        this.router.navigate(['/']);

    }

    toggleMenu() {
        this.menuShown = !this.menuShown;
    }
}
