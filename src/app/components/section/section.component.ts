import {Component, Input} from '@angular/core';

export type SectionType = 'warning' | 'notes' | 'link' | 'question' | 'list' | 'code';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent {
  @Input() type: SectionType = 'notes';

  constructor() {

  }
}