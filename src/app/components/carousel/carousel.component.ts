import {Component, ElementRef, HostListener, ViewChild} from '@angular/core';

@Component({
    selector: 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent{
    pos: number = 0;
    step: number = 0;
    max: number = 0;
    slider!: HTMLInputElement;

    @ViewChild('sliderContainer') sliderContainer!: ElementRef<HTMLInputElement>;
    constructor() { }

    @HostListener('window:resize', ['$event'])
    onResize(event: Event) {
        this.#calcSize();
    }

    onLeftClick() {
        if (this.pos < 0) { this.pos += this.step; }
        this.#setPos(this.pos);
    }

    onRightClick() {
        if (this.pos > -this.max) { this.pos -= this.step; }
        this.#setPos(this.pos);
    }
    #setPos(pos: number) {
        this.pos = pos;
        this.slider.style.left = this.pos+"px";
    }

    ngAfterViewInit() {
        this.slider = this.sliderContainer.nativeElement; // initial value of the slider
        this.#calcSize();
    }

    #calcSize() {
        this.step = this.slider.clientWidth;
        this.max = (this.slider.children.length - 1) * this.step;
    }
}
