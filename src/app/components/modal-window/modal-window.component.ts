import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})
export class ModalWindowComponent {
    @Input() isOpen: boolean = false;
    @Input() isOpen2: boolean = true;
    onClose() {
        this.isOpen = false;
        this.isOpen2 = true;
        setTimeout(() => {
            this.isOpen2 = false;
        }, 500);
        console.info("do close");
    }

    onCollapseEnd(ev: any) {
        console.info("collapse end", ev);
    }
}
