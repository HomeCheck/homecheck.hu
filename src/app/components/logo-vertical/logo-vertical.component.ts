import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-logo-vertical',
  templateUrl: './logo-vertical.component.html',
  styleUrls: ['./logo-vertical.component.scss']
})
export class LogoVerticalComponent {
  @Input() uid: string = "22";
}
