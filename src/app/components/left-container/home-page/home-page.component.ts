import {Component, OnInit} from '@angular/core';

interface Techs {
    name: string;
    icon: string;
    title: string;
    description: string;
}

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
    modalOpen: boolean = false;
    constructor() { }

    ngOnInit(): void {
        this.modalOpen = true;
    }

    techs: Techs[] = [
        {
            name: "typescript",
            icon: "nodejs.svg",
            title: "Typescript and NodeJS",
            description: "The Server and related codes are written in TypeScript on a host of NodeJS. Many other libraries were" +
                "used, and you can find the details in the <a href=\"/server\">Server</a> section."
        }, {
            name: "angular",
            icon: "angular.svg",
            title: "Angular",
            description: "The web and mobile client application is written in Angular, and uses the same code base. The app also uses" +
                " different libraries for state management, routing, etc. You can find more information in the " +
                "<a href=\"/client\">Client</a> section."
        }, {
            name: "cpp",
            icon: "cpp.svg",
            title: "C++",
            description: "The firmware for the devices is written in C++, and uses the Arduino framework. You can find more information in the" +
                " <a href=\"/firmware\">Firmware</a> section."
        }, {
            name: "Fritzing",
            icon: "fritzing.png",
            title: "Fritzing",
            description: "The electronic parts and PCBs are designed in Fritzing. You can find the .fzz files in the" +
                " <a href=\"/fritzing\">Fritzing</a> section. We made all the parts and PCBs available for free use"
        }, {
            name: "openscad",
            icon: "openscad.svg",
            title: "OpenSCAD",
            description: "The 3D printed parts are designed in OpenSCAD. Many of our parts are parametric, so you can easily modify" +
                "them to fit your needs, and they are open source, so you can use them in your own projects." +
                "Some of the electric boards are also designed in OpenSCAD, and you can find the .scad files in the" +
                "<a href=\"/openscad\">OpenSCAD</a> section."
        }, {
            name: "esp32",
            icon: "espressif.svg",
            title: "Espressif: ESP32",
            description: "The target hardware is ESP32 based, and everything is developed for this hardware. You can find here the" +
                "schematics, the PCB design, and the BOM for specific devices. The number of devices is growing."
        }, {
            name: "raspberry-pi",
            icon: "raspberry-pi.svg",
            title: "Raspberry Pi",
            description: "The server application is running on a Raspberry Pi 4, but it can run on any other hardware that can run" +
                "NodeJS. You can find more information in the <a href=\"/server\">Server</a> section."
        }, {
            name: "docker",
            icon: "docker.svg",
            title: "Docker",
            description: "The server application can run in a Docker container. You can find more information in the" +
                "<a href=\"/server\">Server</a> section."
        }, {
            name: "jenkins",
            icon: "jenkins.svg",
            title: "Jenkins",
            description: "We use our own Jenkins server for building the server application and the related web pages along with" +
                "some other tasks. You can find more information in the <a href=\"/jenkins\">Jenkins</a> section."
        }, {
            name: "bash",
            icon: "bash.svg",
            title: "Bash",
            description: "Bash scripts are being used for HomeCheck server host setup and also there are scripts for setting" +
                " up hosting related home pages (<a href=\"https://helyzet.eu\">helyzet.eu</a> and" +
                "<a href=\"https://homecheck.hu\">homecheck.hu</a> so far) and for building the server application."
        }

    ]
}
