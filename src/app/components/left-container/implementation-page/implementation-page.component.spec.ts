import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ImplementationPageComponent} from './implementation-page.component';

describe('ImplementationPageComponent', () => {
    let component: ImplementationPageComponent;
    let fixture: ComponentFixture<ImplementationPageComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ImplementationPageComponent]
        });
        fixture = TestBed.createComponent(ImplementationPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
