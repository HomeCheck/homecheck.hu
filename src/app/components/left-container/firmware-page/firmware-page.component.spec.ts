import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmwarePageComponent } from './firmware-page.component';

describe('FirmwarePageComponent', () => {
  let component: FirmwarePageComponent;
  let fixture: ComponentFixture<FirmwarePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FirmwarePageComponent]
    });
    fixture = TestBed.createComponent(FirmwarePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
