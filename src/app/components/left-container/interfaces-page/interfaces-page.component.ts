import { Component } from '@angular/core';

@Component({
  selector: 'app-interfaces-page',
  templateUrl: './interfaces-page.component.html',
  styleUrls: ['./interfaces-page.component.scss']
})
export class InterfacesPageComponent {
    generalStatus:string = `
    {
        "meta": {
            "requestid": "1122-2212323132-2113",
            "uid": "11-22-33-44"
        },
        "data": {
            <<message data, variable structure>>
        }
    }`;

    infoResponse: string = `
    {
        "meta": {
            "requestId": "1122-2212323132-2113",
            "uid": "11:22:33:44"
        },
        "data": {
            "info": {
                "uid": "11:22:33:44",
                "name": "name",
                "type": "wateringstation",
                "status": "WORK",
                "firmwareVersion": "1.1.2",
                "params": [
                    {
                        "name": "param1",
                        "value": "12"
                    }, 
                    {
                        "name": "param2",
                        "value": "23"
                    },
                ...
                ]
            }
        }
    }`

    statusResponse: string = `
    {
        "meta": {
            "requestId": "1122-2212323132-2113",
            "uid": "11-22-33-44"
        },
        "data": {
            "status": {
                "errorCode": "1111",
                "errorReason": "no wifi connection established",
                "status": "FAILURE",
                "busy": true
            },
            "traits": [
                {
                    "name": "trait1",
                    "value": "111"
                }, 
                {
                    "name": "trait2",
                    "value": "222"
                }, 
                ...
            ],
            "result": {
                "status": "? (ACK|NAK|SUCCESSFUL|FAILURE|OTHER?)",
                "message": "3 params are updated"
            }
        }
    }
    `;
}
