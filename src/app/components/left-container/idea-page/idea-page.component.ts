import {Component} from '@angular/core';
import { HighlightModule } from 'ngx-highlightjs';

@Component({
    selector: 'app-idea-page',
    templateUrl: './idea-page.component.html',
    styleUrls: ['./idea-page.component.scss']
})
export class IdeaPageComponent {
    languages: string[] = ['typescript', 'css', 'cpp'];
    code: string = `
        import {Component} from '@angular/core';
        import { HighlightModule } from 'ngx-highlightjs';
        
        @Component({
            selector: 'app-idea-page',
            templateUrl: './idea-page.component.html',
            styleUrls: ['./idea-page.component.scss']
        });        
    `
}
