import {Component} from '@angular/core';

@Component({
    selector: 'app-structure-page',
    templateUrl: './structure-page.component.html',
    styleUrls: ['./structure-page.component.scss']
})
export class StructurePageComponent {
    public finito(ev: any) {
        console.info(ev);
    }
}
