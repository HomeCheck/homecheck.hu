import { Component } from '@angular/core';

@Component({
  selector: 'app-server-page',
  templateUrl: './server-page.component.html',
  styleUrls: ['./server-page.component.scss']
})
export class ServerPageComponent {
  runtypesExample1: string = `const Planet = Record({
    type: String,
    location: Vector,
    mass: Number,
    population: Number,
    habitable: Boolean,
  });`

  runtypesExample2: string = `type IPlanet = Static<typeof Planet>;`;

  runtypesExample3: string = `interface IPlanet {
    type: string;
    location: Vector;
    mass: number;
    population: number;
    habitable: boolean;
  }`;

  runtypesExample4: string = `
  let data1: any = {
    type: "position";
    location: [5, 3];
    mass: 12;
    population: 12;
    habitable: 12;  // error: boolean expected
  }
  
  let data2 = Planet.check(data1); 
  `;
}
