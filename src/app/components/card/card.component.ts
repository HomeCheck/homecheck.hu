import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent {
    @Input() title: string = ''
    @Input() content: string = '';
    @Input() className: string = '';
    @Input() link: string = '';

    constructor(private router: Router) {
    }

    public onTitleClick(link: any) {
        console.info('link',link)
        // somehow <a> not working if it is not here
        this.router.navigate([link]);
    }
}
