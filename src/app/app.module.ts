import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LogoComponent } from '@components/logo/logo.component';
import { IdeaPageComponent } from '@components/left-container/idea-page/idea-page.component';
import { StructurePageComponent } from '@components/left-container/structure-page/structure-page.component';
import { ImplementationPageComponent } from '@components/left-container/implementation-page/implementation-page.component';
import { InfrastructurePageComponent } from '@components/left-container/infrastructure-page/infrastructure-page.component';
import { HomePageComponent } from '@components/left-container/home-page/home-page.component';
import { HeaderComponent } from '@components/header/header.component';
import { FooterComponent } from '@components/footer/footer.component';
import { MainMenuComponent } from '@components/main-menu/main-menu.component';
import { MainMenuItemComponent } from '@components/main-menu-item/main-menu-item.component';
import { CarouselComponent } from '@components/carousel/carousel.component';
import { TimelineComponent } from '@components/timeline/timeline.component';
import {NgOptimizedImage} from "@angular/common";
import { BubbleComponent } from '@components/bubble/bubble.component';
import { CardComponent } from '@components/card/card.component';
import { SectionComponent } from '@components/section/section.component';
import { InterfacesPageComponent } from '@components/left-container/interfaces-page/interfaces-page.component';
import { UpcomingPageComponent } from '@components/left-container/upcoming-page/upcoming-page.component';
import {Highlight, HIGHLIGHT_OPTIONS} from "ngx-highlightjs";
import { ModalWindowComponent } from './components/modal-window/modal-window.component';
import { ServerPageComponent } from './components/left-container/server-page/server-page.component';
import { FirmwarePageComponent } from './components/left-container/firmware-page/firmware-page.component';
import { ClientPageComponent } from './components/left-container/client-page/client-page.component';
import { HardwarePageComponent } from './components/left-container/hardware-page/hardware-page.component';
import { MobileHeaderComponent } from './components/mobile/mobile-header/mobile-header.component';
import { MobileMainMenuComponent } from './components/mobile/mobile-main-menu/mobile-main-menu.component';
import { LogoVerticalComponent } from '@components/logo-vertical/logo-vertical.component';

@NgModule({
  declarations: [
    AppComponent,
    LogoComponent,
    IdeaPageComponent,
    StructurePageComponent,
    ImplementationPageComponent,
    InfrastructurePageComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    MainMenuComponent,
    MainMenuItemComponent,
    CarouselComponent,
    TimelineComponent,
    BubbleComponent,
    CardComponent,
    SectionComponent,
    InterfacesPageComponent,
    UpcomingPageComponent,
    ModalWindowComponent,
    ServerPageComponent,
    FirmwarePageComponent,
    ClientPageComponent,
    HardwarePageComponent,
    MobileHeaderComponent,
    MobileMainMenuComponent,
    LogoVerticalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgOptimizedImage,
    Highlight
  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js/lib/core'),
        lineNumbersLoader: () => import('ngx-highlightjs/line-numbers'), // Optional, only if you want the line numbers
        languages: {
          typescript: () => import('highlight.js/lib/languages/typescript'),
          json: () => import('highlight.js/lib/languages/json'),
          css: () => import('highlight.js/lib/languages/css'),
          xml: () => import('highlight.js/lib/languages/xml')
        },
        themePath: 'assets/styles/stackoverflow-light.css' // Optional, and useful if you want to change the theme dynamically
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
